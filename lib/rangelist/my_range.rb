class MyRange
  attr_accessor :left, :right
  def initialize left = nil, right = nil
    @left = left
    @right = right
  end

  def empty?
    left >= right
  end

  # 当所有值都小于 range 的值，则返回true
  # [1, 5] < [6, 10]
  def < range
    empty? || range.empty? || (right < range.left)
  end

  # 当所有值都大于 range 的值，则返回true
  # [6, 10] > [1, 5]
  def > range
    empty? || range.empty? || left > range.right
  end

  def == range
    left == range.left && right == range.right
  end

  # 包含range
  def include? range
    left <= range.left && right >= range.right
  end

  # 包含 range#left 
  def linclude? range
    range.left >= left && range.left <= right
  end

  def to_s
    "[#{left}, #{right})"
  end

  def + range
    _range = self
    return _range if range.empty?
    if _range < range
      [_range, range]
    elsif _range > range
      [range, _range]
    elsif _range.include?(range)
      [_range]
    elsif range.include?(_range)
      [range]
    elsif range.linclude?(_range)
      [MyRange.new(range.left, right)]
    elsif _range.linclude?(range)
      [MyRange.new(left, range.right)]
    end
  end

  def - range
    _range = self
    return [_range] if range.empty?
    if _range < range || _range > range
      [_range]
    elsif _range.include?(range)
      [MyRange.new(left, range.left), MyRange.new(range.right, right)].select{|r| !r.empty?}
    elsif range.include?(_range)
      []
    elsif range.linclude?(_range)
      [MyRange.new(range.right, right)].select{|r| !r.empty?}
    elsif _range.linclude?(range)
      [MyRange.new(left, range.left)].select{|r| !r.empty?}
    end
  end
end




