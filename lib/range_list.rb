require 'rangelist/my_range'
class RangeList
  attr_accessor :ranges
  def initialize
    @ranges = []
  end

  def add range
    range = MyRange.new(range.first, range.last)
    return if range.empty?
    results = []
    @ranges.each do |r|
      rs = r + range
      results << rs[0]
      range = rs[1]
      break if range.nil?
    end
    results << range if range.is_a?(MyRange) && !range.empty?
    @ranges = results
  end

  def remove range
    range = MyRange.new(range.first, range.last)
    return if range.empty?
    results = []
    @ranges.each do |r|
      rs = r - range
      rs.each do |_r|
        results << _r
      end
    end
    @ranges = results
  end


  def print
    result = @ranges.map(&:to_s).join(" ")
    puts result
    result
  end
end
