require 'spec_helper'
require 'range_list'
RSpec.describe RangeList, type: :class do
  before do
    @rl = RangeList.new
    @rl.add([6, 10])
  end

  context 'test add' do
    it "add [1, 5]" do
      @rl.add([1, 5])
      expect(@rl.print).to eq("[1, 5) [6, 10)")
    end

    it "add [1, 6]" do
      @rl.add([1, 6])
      puts @rl.ranges
      expect(@rl.print).to eq("[1, 10)")
    end

    it "add [1, 8]" do
      @rl.add([1, 8])
      expect(@rl.print).to eq("[1, 10)")
    end

    it "add [1, 10]" do
      @rl.add([1, 10])
      expect(@rl.print).to eq("[1, 10)")
    end

    it "add [1, 20]" do
      @rl.add([1, 20])
      expect(@rl.print).to eq("[1, 20)")
    end

    it "add [6, 6]" do
      @rl.add([6, 6])
      expect(@rl.print).to eq("[6, 10)")
    end

    it "add [6, 8]" do
      @rl.add([6, 8])
      expect(@rl.print).to eq("[6, 10)")
    end

    it "add [6, 10]" do
      @rl.add([6, 10])
      expect(@rl.print).to eq("[6, 10)")
    end

    it "add [6, 20]" do
      @rl.add([6, 20])
      expect(@rl.print).to eq("[6, 20)")
    end

    it "add [8, 9]" do
      @rl.add([8, 9])
      expect(@rl.print).to eq("[6, 10)")
    end

    it "add [8, 10]" do
      @rl.add([8, 10])
      expect(@rl.print).to eq("[6, 10)")
    end

    it "add [10, 20]" do
      @rl.add([10, 20])
      expect(@rl.print).to eq("[6, 20)")
    end

    it "add [11, 20]" do
      @rl.add([11, 20])
      expect(@rl.print).to eq("[6, 10) [11, 20)")
    end

    it 'add more than one time' do
      @rl.add([1, 5])
      @rl.add([10, 15])
      @rl.add([20, 25])
      expect(@rl.print).to eq("[1, 5) [6, 15) [20, 25)")
    end
  end

  context 'test remove' do
    it "remove [1, 5]" do
      @rl.remove([1, 5])
      expect(@rl.print).to eq("[6, 10)")
    end

    it "remove [1, 6]" do
      @rl.remove([1, 6])
      expect(@rl.print).to eq("[6, 10)")
    end

    it "remove [1, 8]" do
      @rl.remove([1, 8])
      expect(@rl.print).to eq("[8, 10)")
    end

    it "remove [1, 10]" do
      @rl.remove([1, 10])
      expect(@rl.print).to eq("")
    end

    it "remove [1, 20]" do
      @rl.remove([1, 20])
      expect(@rl.print).to eq("")
    end

    it "remove [6, 6]" do
      @rl.remove([6, 6])
      expect(@rl.print).to eq("[6, 10)")
    end

    it "remove [6, 8]" do
      @rl.remove([6, 8])
      expect(@rl.print).to eq("[8, 10)")
    end

    it "remove [6, 10]" do
      @rl.remove([6, 10])
      expect(@rl.print).to eq("")
    end

    it "remove [6, 20]" do
      @rl.remove([6, 20])
      expect(@rl.print).to eq("")
    end

    it "remove [8, 9]" do
      @rl.remove([8, 9])
      expect(@rl.print).to eq("[6, 8) [9, 10)")
    end

    it "remove [8, 10]" do
      @rl.remove([8, 10])
      expect(@rl.print).to eq("[6, 8)")
    end

    it "remove [10, 20]" do
      @rl.remove([10, 20])
      expect(@rl.print).to eq("[6, 10)")
    end

    it "remove [11, 20]" do
      @rl.remove([11, 20])
      expect(@rl.print).to eq("[6, 10)")
    end

    it 'remove' do
      @rl.add([1, 5])
      @rl.add([10, 15])
      @rl.add([20, 25])
      @rl.remove([8, 23])
      expect(@rl.print).to eq("[1, 5) [6, 8) [23, 25)")
    end
  end
end
