# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |s|
  s.name = 'rangelist'
  s.version = '0.0.1'

  s.required_ruby_version = '>= 2.4.0'
  s.required_rubygems_version = Gem::Requirement.new('>= 0') if s.respond_to? :required_rubygems_version=
  s.authors = ['liuhuyang']
  s.description = 'RangeList'
  s.email = ['lhy20062008@126.com']
  s.extra_rdoc_files = ['README.md']
  s.files = `git ls-files -z LICENSE.txt *.md *.gemspec bin lib`.split("\x0")
  s.homepage = 'https://gitlab.com/lhy20062008/rangelist'
  s.licenses = ['Ruby']
  s.require_paths = ['lib']
  s.rubygems_version = '0.1.1'
  s.summary = 'RangeList'
  s.test_files = s.files.grep(/^(test|spec|features)\//)
  s.metadata = { "github_repo" => "ssh://gitlab.com/lhy20062008/rangelist" }
end
